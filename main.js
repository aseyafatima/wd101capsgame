let c = document.getElementById("my-canvas");
let ctx = c.getContext("2d");
let x=0;
//var x = c.width;
let y = 0;
let loadImage = (src,callback)=>{
    let img = document.createElement("img");
    img.onload=()=>callback(img);
    img.src=src;
};

let imagePath = (frameNumber,animation)=>{
    return "/images/"+animation+"/"+frameNumber+".png";

};

let frames={
    idle:[1,2,3,4,5,6,7,8],
    kick:[1,2,3,4,5,6,7],
    punch:[1,2,3,4,5,6,7],
    forward:[1,2,3,4,5,6],
    backward:[1,2,3,4,5,6],
    block:[1,2,3,4,5,6,7,8,9],

};
let loadImages = (callback)=>{
    let images ={idle:[],kick:[],punch:[],forward:[],backward:[],block:[]};
    let imagesToLoad=0;

    ["idle","kick","punch","forward","backward","block"].forEach((animation)=>{
        let animationFrames=frames[animation];
        imagesToLoad=imagesToLoad+animationFrames.length;
        animationFrames.forEach((frameNumber)=>{
            let path = imagePath(frameNumber,animation);
            loadImage(path,(image)=>{
                images[animation][frameNumber-1]=image;
                imagesToLoad-=1;

                if(imagesToLoad===0){
                    callback(images);
                }
            });
            
        })

    })
    
};

let animate=(ctx,images,animation,callback)=>{
    images[animation].forEach((image,index)=>{
        setTimeout(()=>{
            ctx.clearRect(x,0,500,500);
            
            ctx.drawImage(image,x,0,500,500);
            if(animation=="forward"){
            if(x+4<=c.width)
            x += 4;
 }
 if(animation=="backward"){
    if(x+4>=0)
    x -= 4;
}
        
        },index*100);
    });
    setTimeout(callback,images[animation].length*100);
};

loadImages((images)=>{
    let queuedAnimations=[]
    
    let aux = ()=>{
        let selectedAnimation;

        if(queuedAnimations.length===0){
            selectedAnimation="idle";
        }
        else{
            selectedAnimation=queuedAnimations.shift();
        }
        

    animate(ctx,images,selectedAnimation,aux);
    };
    aux();

    document.getElementById("kick").onclick=()=>{
        queuedAnimations.push("kick");
    };
    
    document.getElementById("punch").onclick=()=>{
        queuedAnimations.push("punch");
    };
    document.getElementById("forward").onclick=()=>{
        queuedAnimations.push("forward");
        x=x+1;
    };
    document.getElementById("backward").onclick=()=>{
        queuedAnimations.push("backward");
        if(x-1>0)
        x=x-1;
    };
    document.getElementById("block").onclick=()=>{
        queuedAnimations.push("block");
    };
    
document.addEventListener('keyup', (event) =>{
    const key = event.key; // "ArrowRight", "ArrowLeft", "ArrowUp", or "ArrowDown"
    if(key==="ArrowLeft"){
        queuedAnimations.push("kick");
    }else if(key==="ArrowRight"){
    queuedAnimations.push("punch");}
    else if(key==="ArrowUp"){
        queuedAnimations.push("forward");}
        else if(key==="ArrowDown"){
            queuedAnimations.push("backward");}
            else if(event.code === 'Space'){
                console.log("space is pressed");
                queuedAnimations.push("block");}
});
 
});